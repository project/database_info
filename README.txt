CONTENTS OF THIS FILE
---------------------
 
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 
 
INTRODUCTION
------------
 
The Database Info module provides a Drupal UI to view Default Drupal database
tables.
It doesn't provide the ability to alter or any changes in database. It lists
the default database tables and their data.
 
 * For a full description of the module visit:
   https://www.drupal.org/project/database_info

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/database_info
 

REQUIREMENTS
------------
 
This module requires no modules outside of Drupal core.
 
 
INSTALLATION
------------
 
 * Install the Database Info module as you would normally install a contributed
   Drupal module. Visit https://www.drupal.org/node/1897420 for further
   information.
 
 

CONFIGURATION
-------------
 
    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > Database info > Database to
       view the list of database tables.
    3. The user can use the search box to narrow down the view. Select the
       database table to see the full details of DB table with content.
 
 
MAINTAINERS
-----------
 
8.x-5.x Developed and maintained by:
 * Gaurav jhaloya (https://www.drupal.org/user/3577312)
 * Rahul Patidar (https://www.drupal.org/user/3527897)
